#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*Problema celor 8 regine: să se scrie un program care plasează 8 regine pe tabla de șah, fără ca
acestea să se atace reciproc. Problema a fost investigată de Carl Friedrich Gauss în 1850 și
până astăzi aceasta nu are o soluție analitică ci prin încercări. Problema are 92 de soluții din
care 12 sunt distincte (din motive de simetrie).*/

void reset(char m[9][9], int n)
{
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            m[i][j] = '-';
        }
    }
}

void afisare(char m[9][9], int n)
{
    for (int i = 1 ; i <=   n; i++)
    {
        for (int j = 1; j <=n; j++)
        {
            printf("%c ", m[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    reset(m, n);
}

void Init(int k, int *v) // k – vârful stivei
{
    v[k] = 0; // iniţializează/resetează, valoarea din // vârful stivei
}

int Succesor(int k, int n, int *v)
{
    if (v[k] < n)
    {
        v[k]++;
        return 1; 
    }
    else
    {
        return 0; 
    }
}

int Valid(int k, int *v)
{
    for (int i = 1; i < k; i++)
    {
        if (v[i] == v[k] || i == k || (abs(v[i] - v[k]) == abs(i - k)))
        {
            return 0;
        }
    }
    return 1;
}

int Solution(int k, int n)
{
    return (k == n);
}

void Print(int n, int *m, int *v, char a[9][9])
{

    printf("%d : ", ++(*m));
    for (int k = 1; k <= n; k++)
    {
        printf("%d ", v[k]);
        a[k][v[k]] = '*';
    }
    printf("\n");
    afisare(a, n);
}

void back(int n, char a[9][9])
{
    int v[10]; //stiva
    int k = 1; //varful 
    int m = 0;
    Init(k, v);
    int cnt = 0;
    while (k > 0)
    {
        int isS = 0, isV = 0;
        if (k <= n)
        {
            do
            {
                isS = Succesor(k, n, v);
                if (isS != 0)
                {
                    isV = Valid(k, v);
                }
            } while (isS && !isV);
        }
        if (isS != 0)
        {
            if (Solution(k, n))
            {
                Print(n, &m, v, a);
                cnt++;
            }
            else
            {
                k++;
                Init(k, v);
            }
        }
        else
        {
            k--;
        }
    }
    printf("Numarul total de posibilitati in care poti aranja %d regine pe o tabla [%d]x[%d] este de %d ori\n", n, n, n, cnt);
}

int main()
{
    int n=8;
    // printf("n=");
    // scanf("%d", &n);
    // while (n > 8)
    // {
    //     printf("N nu poate fi de cat maxim 8");
    //     printf("n=");
    //     scanf("%d", &n);
    // }
    char m[9][9];
    reset(m, n);
    back(n, m);
    //printf("programul s a incheiat");
}