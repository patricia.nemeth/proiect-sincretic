# proiect-sincretic MAP



## Problema celor 8 regine

Problema damelor (sau problema reginelor) tratează plasarea a opt regine de șah pe o tablă de șah astfel încât să nu existe două regine care se amenință reciproc. Astfel, se caută o soluție astfel încât nicio pereche de două regine să nu fie pe același rând, pe aceeași coloană, sau pe aceeași diagonală.
Compozitorul de șah⁠(d) Max Bezzel⁠(d) a publicat problema celor opt regine în 1848. Franz Nauck a publicat primele soluții în 1850.[2] Nauck a și extins problema la n regine, cu n regine pe o tablă de șah de n × n pătrate.

## Detalii având în vedere implementarea codului

### Structuri de Date:
Matricea m[9][9] reprezintă tabla de șah, inițializată cu caractere dash ('-').
Implementarea folosește un vector pentru a reprezenta poziția reginei pe fiecare coloană.

### Funcția reset

Resetează tabla de șah la starea inițială, unde toate pozițiile sunt goale.

### Funcția afisare

Afișează tabla de șah curentă. Aceasta este folosită pentru a vizualiza soluțiile găsite.

### Funcția Init

Inițializează vârful stivei pentru a începe explorarea în adâncime.

### Funcția Succesor

Generează un succesor pentru vârful curent al stivei, reprezentând o posibilă poziție pentru regină.

### Funcția Valid

Verifică dacă o poziție pentru regină este validă, adică nu se atacă reciproc cu reginele plasate anterior.

### Funcția Solution

Verifică dacă soluția este completă, adică toate cele 8 regine au fost plasate.

### Funcția Print

Afișează o soluție găsită, marcând pozițiile reginelor pe tabla de șah și incrementând un contor pentru numărul total de soluții.

### Funcția back

Implementează algoritmul de backtracking pentru rezolvarea problemei celor 8 regine.

### Funcția main

Inițializează tabla de șah și apelează funcția back pentru a rezolva problema.

### Limitări și Îmbunătățiri:
Implementarea curentă are o limitare la dimensiunea 8x8 a tablei de șah.
Îmbunătățiri pot fi aduse pentru gestionarea altor dimensiuni sau pentru a îmbunătăți eficiența algoritmului.

## Mediul de Dezvoltare

Proiectul a fost dezvoltat într-un mediu de dezvoltare integrat (IDE) în limbajul de programare C. A fost utilizată o combinație de compilator și editor pentru a dezvolta și testa codul. Aici sunt detaliile mediului de dezvoltare:

Limbaj de Programare: C
IDE: Intellij Code::Blocks
Compilator: GCC (GNU Compiler Collection)

## Motivarea și Scopul Proiectului

### Motivarea:
Proiectul abordează problema clasică a celor 8 regine pe tabla de șah, o problemă cunoscută pentru complexitatea și aplicabilitatea ei în domeniul algoritmilor de backtracking. Motivul principal este de a oferi o soluție eficientă și vizualizare a tuturor aranjamentelor posibile ale reginelor pe tabla de șah.

### Scopul:
Scopul proiectului este de a implementa și prezenta o soluție în limbajul de programare C folosind algoritmul de backtracking pentru a găsi toate pozițiile posibile pentru 8 regine astfel încât să nu se atace reciproc. De asemenea, proiectul oferă o vizualizare a acestor soluții pe o tablă de șah și furnizează informații despre numărul total de aranjamente posibile.

## Exemple de Rulare
```
int main() {
    int n = 8;
    char m[9][9];
    reset(m, n);
    back(n, m);
    printf("Programul s-a încheiat.\n");
    return 0;
}
```
Exemplul de mai sus utilizează funcția 'back' pentru a rezolva problema celor 8 regine pe o tablă de șah de dimensiune 8x8.

## Livrabil

Imagina Docker poate fi construită și accesată de pe [Docker Hub][https://hub.docker.com/repository/docker/patricianemeth/dame/general]. Pentru a utiliza această imagine, rulează următoarele comenzi:

```
docker pull nume_utilizator/nume_repository
docker run -it nume_utilizator/nume_repository
```
## Bibliografie
wikipedia




[def]: https://hub.docker.com/repository/docker/patricianemeth/dame/general