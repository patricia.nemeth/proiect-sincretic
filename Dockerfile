FROM gcc:latest
WORKDIR /usr/src/myapp
COPY sah.c .
RUN gcc -o myapp sah.c -lstdc++
CMD ["./myapp"]